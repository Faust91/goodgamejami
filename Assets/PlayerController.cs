using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(InputReader))]
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
    [RequireComponent(typeof(PlayerInput))]
#endif


public class PlayerController : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        inputReader = GetComponent<InputReader>();
        mushroomInteractionManager.OnMushroomInteraction += handleGathering;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
        _playerInput = GetComponent<PlayerInput>();
#endif
        CurrentSpeed = 0;
        acceleration = MaxAccelerationSpeed / TimeToReachMaxSpeed;
        speedIncreseTimers = new List<float>();
    }

    // Update is called once per frame
    void Update()
    {
        setLastInputAxis();
        if (isAccelerating())
        {
            accelerate();
            rotate();
        }
        else if (CurrentSpeed > 0)
        {
            decelerate();
            rotate();
        }
        move();
    }

    public void handleGathering()
    {
        updateSpeed(SpeedIncrease);
        numberOfIncreasedSpeedActive++;
        speedIncreseTimers.Add(Time.time);
    }

    private void checkSpeedTimers()
    {
        if (speedIncreseTimers.Count > 0)
        {
            float currentDelta = Time.time - speedIncreseTimers[0];
            if (currentDelta >= TimeOfIncreasedSpeed)
            {
                updateSpeed(-SpeedIncrease);
                numberOfIncreasedSpeedActive--;
                speedIncreseTimers.RemoveAt(0);
            }
        }
    }

    private void updateSpeed(float increasedSpeed)
    {
        MaxAccelerationSpeed += increasedSpeed;
        Mathf.Clamp(MaxAccelerationSpeed, 0, CapSpeed);
    }

    private bool hasAxisInput()
    {
        return getLeftStickAxis().x != 0 || getLeftStickAxis().y != 0;
    }
    private bool isAccelerating()
    {
        return inputReader.isAccelerate;
    }
    private void move()
    {
        float currRotAngle = Mathf.Atan2(getLeftStickAxis().x, getLeftStickAxis().y) * Mathf.Rad2Deg;
        float rotationAngleAbs = currRotAngle > 0 ? currRotAngle : -currRotAngle;
        float angularSpeed = angularSpeedCurve.Evaluate(rotationAngleAbs);
        float instantSpeed = CurrentSpeed * angularSpeed;
        controller.Move(transform.forward * Time.deltaTime * instantSpeed);
    }
    private void rotate()
    {
        rotationAngle = Mathf.Atan2(getLeftStickAxis().x, getLeftStickAxis().y) * Mathf.Rad2Deg;
        float currentRotationSpeed = RotationSpeedCurve.Evaluate(rotationAngle);
        transform.Rotate(Vector3.up, rotationAngle * Time.deltaTime * currentRotationSpeed);
    }
    private void setLastInputAxis()
    {
        lastInputAxis = new Vector3(getLeftStickAxis().x, 0f, getLeftStickAxis().y);
    }

    private Vector2 getLeftStickAxis()
    {
        return inputReader.axisInput;
    }

    private void accelerate()
    {
        if (CurrentSpeed - (IncreasedSpeed * numberOfIncreasedSpeedActive) < MaxAccelerationSpeed)
        {
            CurrentSpeed += getDeltaAcceleration();
            if (CurrentSpeed > MaxAccelerationSpeed)
            { CurrentSpeed = MaxAccelerationSpeed; }
        }
    }

    private void decelerate()
    {
        if (CurrentSpeed > 0)
        {
            float deltaAcceleration = getDeltaDeceleration();
            if (deltaAcceleration < CurrentSpeed)
                CurrentSpeed -= deltaAcceleration;
            else
                CurrentSpeed = 0;
        }
    }
    private float getDeltaAcceleration()
    {
        return acceleration * Time.deltaTime;
    }
    private float getDeltaDeceleration()
    {
        return deceleration * Time.deltaTime;
    }

    public MushroomInteractionManager mushroomInteractionManager;
    [Header("Player speed")]
    [Tooltip("Max speed of the character in m/s")]
    public float MaxAccelerationSpeed = 4.0f;
    public float CapSpeed = 75f;
    [Tooltip("Time to reach max speed in seconds")]
    public float TimeToReachMaxSpeed = 4f;
    public float deceleration = 1;
    public AnimationCurve RotationSpeedCurve;
    public AnimationCurve angularSpeedCurve;
    //meters per second

    [Header("Speed BOOST")]
    public float SpeedIncrease;
    private float TimeOfIncreasedSpeed;
    [Header("DEBUG boost")]
    public float IncreasedSpeed;
    public int numberOfIncreasedSpeedActive;
    public List<float> speedIncreseTimers;

    [Header("DEBUG")]
    [Header("DEBUG movement")]
    public float rotationSpeed;
    public float CurrentSpeed;
    public float acceleration = 1;
    public Vector3 lastInputAxis;
    public float rotationAngle;

    private CharacterController controller;
    private InputReader inputReader;

    [SerializeField] private GameObject mushroomInteractionManagerGO;

};
