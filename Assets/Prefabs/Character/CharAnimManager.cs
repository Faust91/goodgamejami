using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharAnimManager : MonoBehaviour
{
    [Header("Anim properties")]
    public Transform playerParent;
    public Transform root;
    public Transform head;
    public Rigidbody charRb;
    public GameObject rigParent;
    public float physicSpeed = 1000f;
    public float maxHeadRotationFeedback = 90f;

    private Vector3 prevPos;
    private Vector3 targetRotation;
    private Vector3 previousRotation;
    private Vector3 startHeadOffset;
    private Vector3 startForward;
    private float currentRotation;
    private float previousSpeed;
    private PlayerController playerCTRL;
    private AIController AI_CTRL;
    /**/


    private void Start()
    {
        rigParent.SetActive(true);
        startHeadOffset = head.position - root.position;
        playerCTRL = playerParent.GetComponent<PlayerController>();
        AI_CTRL = playerParent.GetComponent<AIController>();
    }


    private void FixedUpdate()
    {

        float currentSpeed = 0f;
        float maxAccelerationSpeed = 0;
        bool isAccelerating = true;

        if (playerCTRL != null)
        {
            currentSpeed = playerCTRL.CurrentSpeed;
            maxAccelerationSpeed = playerCTRL.MaxAccelerationSpeed;
            isAccelerating = playerParent.GetComponent<InputReader>().isAccelerate;
        }
        else if (AI_CTRL != null)
        {
            currentSpeed = AI_CTRL.CurrentSpeed;
            maxAccelerationSpeed = AI_CTRL.MaxSpeed;
        }

        float speedRatio = (currentSpeed / maxAccelerationSpeed);

        if (isAccelerating)
        {
            if(playerCTRL != null)
            {
                charRb.AddForce(-playerParent.forward * physicSpeed * speedRatio);
            }
                
            else if (AI_CTRL != null)
            {
                charRb.AddForce(-playerParent.forward * physicSpeed * speedRatio * Random.Range(0.4f, 1f));
            }
            charRb.AddForce(-playerParent.up * 5f * speedRatio);
            if (playerCTRL != null)
                charRb.AddForce(-playerParent.right * physicSpeed*0.1f * playerParent.GetComponent<InputReader>().axisInput.x * speedRatio);
        }

        previousSpeed = currentSpeed;

        float newRotation = 0f;

        Debug.Log("Speed Ratio " + speedRatio);

        if (isAccelerating)
        {
            float rotationMultiplier = 0f;
            if (speedRatio > 0.7f)
            {
                rotationMultiplier = 1f;
            }
            else if (speedRatio > 0.5f)
            {
                rotationMultiplier = 0.7f;
            }
            else if (speedRatio > 0.2f)
            {
                rotationMultiplier = 0.5f;
            }

            newRotation = maxHeadRotationFeedback * rotationMultiplier;

        }

        currentRotation = Mathf.Lerp(currentRotation, newRotation, 2f*Time.deltaTime);

        head.localEulerAngles = new Vector3(head.localEulerAngles.x, -currentRotation, head.localEulerAngles.z);

    }
}
