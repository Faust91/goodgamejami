using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private int gameplayScene;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("A"))
            SceneManager.LoadScene(gameplayScene);
        if (Input.GetButtonDown("B"))
            QuitGame();
    }

    public static void QuitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }
}
