using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_rotateby : MonoBehaviour
{
    public float Vel;
    CharacterController rb;
    GameObject bone;
    // Start is called before the first frame update
    void Start()
    {
         rb = GetComponent<CharacterController>();
         bone = rb.transform.Find("def_stem_02").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        Vel = rb.velocity.magnitude;
        bone.transform.Rotate(-30,0,0*Vel);
    }
}
