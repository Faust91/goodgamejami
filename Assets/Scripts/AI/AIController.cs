using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

[RequireComponent(typeof(CharacterController))]
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
    [RequireComponent(typeof(PlayerInput))]
#endif
public class AIController : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
        _playerInput = GetComponent<PlayerInput>();
#endif
        CurrentSpeed = 0;
        acceleration = MaxSpeed / TimeToReachMaxSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        setLastInputAxis();
        if (isAccelerating())
        {
            accelerate();
            rotate();
        }
        else if (CurrentSpeed > 0)
        {
            decelerate();
            rotate();
        }
        move();
    }

    private bool hasAxisInput()
    {
        return getLeftStickAxis().x != 0 || getLeftStickAxis().y != 0;
    }
    private bool isAccelerating()
    {
        return true;
    }
    private void move()
    {
        float currRotAngle = Mathf.Atan2(getLeftStickAxis().x, getLeftStickAxis().y) * Mathf.Rad2Deg;
        float rotationAngleAbs = currRotAngle > 0 ? currRotAngle : -currRotAngle;
        float angularSpeed = angularSpeedCurve.Evaluate(rotationAngleAbs);
        float instantSpeed = CurrentSpeed * angularSpeed;
        controller.Move(transform.forward * Time.deltaTime * instantSpeed);
    }
    private void rotate()
    {
        rotationAngle = Mathf.Atan2(getLeftStickAxis().x, getLeftStickAxis().y) * Mathf.Rad2Deg;
        float currentRotationSpeed = RotationSpeedCurve.Evaluate(rotationAngle);
        transform.Rotate(Vector3.up, rotationAngle * Time.deltaTime * currentRotationSpeed);
    }
    private void setLastInputAxis()
    {
        lastInputAxis = new Vector3(getLeftStickAxis().x, 0f, getLeftStickAxis().y);
    }

    private Vector2 getLeftStickAxis()
    {
        return new Vector2(0,0);
    }

    private void accelerate()
    {
        if(CurrentSpeed < MaxSpeed)
        {
            CurrentSpeed += getDeltaAcceleration();
            if (CurrentSpeed > MaxSpeed)
            { CurrentSpeed = MaxSpeed; }
        }
    }
    
    private void decelerate()
    {
        if(CurrentSpeed > 0)
        {
            float deltaAcceleration = getDeltaAcceleration();
            if (deltaAcceleration < CurrentSpeed)
                CurrentSpeed -= deltaAcceleration;
            else
                CurrentSpeed = 0;
        }
    }
    private float getDeltaAcceleration()
    {
        return acceleration * Time.deltaTime;
    }

    [Header("Player speed")]
    [Tooltip("Max speed of the character in m/s")]
    public float MaxSpeed = 4.0f;
    [Tooltip("Time to reach max speed in seconds")]
    public float TimeToReachMaxSpeed = 4f;
    public AnimationCurve RotationSpeedCurve;
    public AnimationCurve angularSpeedCurve;
    //meters per second

    [Header("DEBUG")]
    public float rotationSpeed;
    public float CurrentSpeed;
    public float acceleration;
    public Vector3 lastInputAxis;
    public float rotationAngle;

    private CharacterController controller;
    private InputReader inputReader;
};
