using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISpawnManager : MonoBehaviour
{
    [SerializeField] private int ConcurrentAIs = 10;
    [SerializeField] private float minAISpeed = 8f;
    [SerializeField] private float maxAISpeed = 16f;
    [SerializeField] private int minAISizeMultiplier = 1;
    [SerializeField] private int maxAISizeMultiplier = 2;

    private AISpawnPoint[] spawnPoints;

    // Start is called before the first frame update
    void Start()
    {

        spawnPoints = FindObjectsOfType<AISpawnPoint>();

        int safeConcurrentAIs = Mathf.Min(ConcurrentAIs, spawnPoints.Length);

        if (ConcurrentAIs > spawnPoints.Length)
            Debug.LogWarning("Concurrent AI are more than current Spawn Point. One AI is spawned for each spawn point.");
        
        for (int i=0 ; i < safeConcurrentAIs; i++)
        {
            spawnPoints[i].Spawn(Random.Range(minAISpeed, maxAISpeed),
                                 Random.Range(minAISizeMultiplier, maxAISizeMultiplier)
                                 );
        }
    }

    private void SpawnAI()
    {
        int i = Random.Range(0, spawnPoints.Length);
        spawnPoints[i].Spawn(Random.Range(minAISpeed, maxAISpeed),
                             Random.Range(minAISizeMultiplier, maxAISizeMultiplier)
                             );

    }

    public void NotifyRootedAI()
    {
        SpawnAI();
    }
}
