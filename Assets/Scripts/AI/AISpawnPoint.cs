using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISpawnPoint : MonoBehaviour
{
    [SerializeField] GameObject AI;

    // Start is called before the first frame update
    void Start()
    {
        RemoveVisualOnStart();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Spawn(float Speed, int scale)
    {
        AIController ai = Instantiate(AI, transform.position, transform.rotation).GetComponent<AIController>();
        ai.MaxSpeed = Speed;
        ai.transform.localScale *= scale;
    }

    private void RemoveVisualOnStart()
    {
        SkinnedMeshRenderer[] visuals = GetComponentsInChildren<SkinnedMeshRenderer>();

        foreach (SkinnedMeshRenderer v in visuals)
        {
            v.enabled = false;
        }
    }
}
