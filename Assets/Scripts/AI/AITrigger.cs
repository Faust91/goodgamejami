using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITrigger : MonoBehaviour
{

    [SerializeField] GameObject rootedMushroom;

    private MushroomInteractionManager scoreManager;
    private AISpawnManager spawnManager;
    //private GameObject manager;

    // Start is called before the first frame update
    void Start()
    {
        scoreManager = FindObjectOfType<MushroomInteractionManager>();
        spawnManager = FindObjectOfType<AISpawnManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>())
        {
            scoreManager.HandleInteraction();
            spawnManager.NotifyRootedAI();

            Destroy(gameObject);
            Instantiate(rootedMushroom, transform.position, transform.rotation);
        }
    }
}
