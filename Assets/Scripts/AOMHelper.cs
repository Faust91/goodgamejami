using System;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;

public class AOMHelper : MonoBehaviour
{
    const float METERS_PER_FRAME = 1.0f;

    private List<AreaOfMovementData> _AOMDataList = new List<AreaOfMovementData>();
    private AreaOfMovementData _currentAOM = null;
    private AreaOfMovementData _lastValidAOM = null;

    private List<AreaOfMovementData> _currentAOMs = new List<AreaOfMovementData>();
    private List<AreaOfMovementData> _nextAOMs = new List<AreaOfMovementData>();

    private void Update()
    {
        UpdateAOM(transform);
    }

    public bool TryAddAOM(AreaOfMovementData aom)
    {
        if (!_AOMDataList.Contains(aom))
        {
            _AOMDataList.Add(aom);
            return true;
        }
        return false;
    }

    public void UpdateAOM(Transform playerTransform)
    {
        _currentAOMs.Clear();
        _nextAOMs.Clear();

        foreach (AreaOfMovementData aom in _AOMDataList)
        {
            if (IsInsideAOM(playerTransform.position, aom))
            {
                _currentAOMs.Add(aom);
                Vector3 nextPlayerPosition = playerTransform.position + playerTransform.forward * METERS_PER_FRAME;
                if (IsInsideAOM(nextPlayerPosition, aom))
                {
                    _nextAOMs.Add(aom);
                }
            }
        }
        _currentAOM = null;
        if (_nextAOMs.Count > 0)
        {
            _nextAOMs.Sort((a1, a2) => Mathf.RoundToInt((a1.GetCenter() - playerTransform.position).sqrMagnitude - (a2.GetCenter() - playerTransform.position).sqrMagnitude));
            _currentAOM = _nextAOMs[0];
        }
        else if (_currentAOMs.Count > 0)
        {
            _currentAOMs.Sort((a1, a2) => Mathf.RoundToInt((a1.GetCenter() - playerTransform.position).sqrMagnitude - (a2.GetCenter() - playerTransform.position).sqrMagnitude));
            _currentAOM = _currentAOMs[0];
        }
        if (null != _lastValidAOM && _currentAOM != _lastValidAOM)
        {
            _lastValidAOM.SetCurrent(false);
        }
        if (null != _currentAOM)
        {
            _currentAOM.SetCurrent(true);
            _lastValidAOM = _currentAOM;
        }
        Debug.Log("Current AOM: " + _currentAOM);
        Debug.Log("Last valid AOM: " + _lastValidAOM);
    }

    public bool IsInsideAOM(Vector3 position, AreaOfMovementData aom)
    {
        return (aom.GetCenter() - position).sqrMagnitude <= aom.GetRadius() * aom.GetRadius();
    }

    public AreaOfMovementData GetCurrentAOM()
    {
        return _currentAOM;
    }

    public AreaOfMovementData GetLastValidAOM()
    {
        return _lastValidAOM;
    }


#if UNITY_EDITOR
    private void OnGUI()
    {
        String explorableAreasString = "";
        foreach (AreaOfMovementData aom in _AOMDataList)
        {
            explorableAreasString += ("Unlocked area: " + aom.ToString() + "\n");
        }
        GUI.color = Color.white;
        GUI.Label(new Rect(20, 20, 800, 600), explorableAreasString);
    }
#endif
}
