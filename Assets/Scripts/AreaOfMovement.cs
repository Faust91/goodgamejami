using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AreaOfMovement : MonoBehaviour
{
    [SerializeField] private AreaSizeScriptableObject _areaSize;
    [SerializeField] private Transform _AOMSphere;

    public AreaSizeScriptableObject GetAreaSize() { return _areaSize; }

    private void Awake()
    {
        if (null == _areaSize)
        {
            Debug.LogError("Area size not found!");
        }
        if (null == _AOMSphere)
        {
            Debug.LogError("AOM sphere not found!");
        }
        _AOMSphere.localScale = Vector3.one * 2.0f * _areaSize.radius;

    }

    private void OnDrawGizmos()
    {
        if (null != _areaSize)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, _areaSize.radius);
        }
        Gizmos.color = Color.white;
    }
}
