using UnityEngine;
using System;

public class AreaOfMovementData : IEquatable<AreaOfMovementData>
{
    private Vector3 _center = Vector3.zero;
    private float _radius = 0.0f;

    private GameObject _AOMGO;

    public Vector3 GetCenter() { return _center; }
    public float GetRadius() { return _radius; }

    public void SetCurrent(bool current)
    {
        if (current)
        {
            _AOMGO.GetComponentInChildren<MeshRenderer>().material.SetFloat("_TransparencyFactor", 1.1f);
        }
        else
        {
            _AOMGO.GetComponentInChildren<MeshRenderer>().material.SetFloat("_TransparencyFactor", 1.01f);
        }
    }

    public AreaOfMovementData(AreaOfMovement areaOfMovement)
    {
        _AOMGO = areaOfMovement.gameObject;
        _center = areaOfMovement.transform.position;
        _radius = areaOfMovement.GetAreaSize().radius;
    }

    public bool Equals(AreaOfMovementData other)
    {
        return _center.Equals(other.GetCenter()) && _radius == other.GetRadius();
    }

    public override string ToString()
    {
        return string.Format("center:[{0:0.00}, {1:0.00}, {2:0.00}], radius:[{3:0.00}]", _center.x, _center.y, _center.z, _radius);
    }
}
