using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class AttachToGround : MonoBehaviour
{


    [Header("Tuning")]
    [Tooltip("Snap position relative to the transform of")]
    public Vector3 attachmentOffset;
    [Tooltip("Multiplier of the attachment offset")]
    public float rayLength = 0.0f;
    public float rayAngle = 0.0f;
    public int rayCount = 0;
    [Tooltip("Multiplier of the attachment offset")]
    public float outerRayLength = 0.0f;
    public float outerRayAngle = 0.0f;
    public int outerRayCount = 0;
    [Tooltip("Multiplier of the attachment offset")]
    public float middleRayLength = 0.0f;
    public float positionDampening = 0.0f;
    public float rotationDampening = 0.0f;
    public LayerMask layerMask;

    [Header("DEBUG")]
    public int hitCountDbg = 0;

    int CastRays(int startIdx, int count, float length, float angle, Vector3 down, ref RaycastHit[] hits, ref Vector3 targetNormal, ref Vector3 targetPos)
    {
        float rayStep = 360.0f / count;
        int hitCount = 0;

        for (int i = startIdx; i < startIdx + count; i++)
        {
            Vector3 rayDir = Quaternion.AngleAxis(angle, transform.right) * down;
            rayDir = Quaternion.AngleAxis(rayStep * i, down) * rayDir;

            if (Physics.Raycast(transform.position, rayDir, out hits[i], attachmentOffset.magnitude * length, layerMask))
            {
                Debug.DrawRay(transform.position, rayDir * hits[i].distance, Color.yellow);
                hitCount++;
                targetNormal += hits[i].normal;
                targetPos += hits[i].point;
            }
        }

        return hitCount;
    }

    void LateUpdate()
    {
        RaycastHit[] hits = new RaycastHit[rayCount + outerRayCount + 1];

        float rayStep = 360.0f / rayCount;
        int hitCount = 0;

        Vector3 targetNormals = Vector3.zero;
        Vector3 targetPositions = Vector3.zero;

        Vector3 down = transform.up * -1;

        hitCount += CastRays(0, rayCount, rayLength, rayAngle, down, ref hits, ref targetNormals, ref targetPositions);
        hitCount += CastRays(rayCount, outerRayCount, outerRayLength, outerRayAngle, down, ref hits, ref targetNormals, ref targetPositions);
        hitCount += CastRays(rayCount + outerRayCount, 1, middleRayLength, 0.0f, down, ref hits, ref targetNormals, ref targetPositions);

        if (hitCount > 0)
        {
            Vector3 targetNormalAvg = targetNormals / hitCount;
            Vector3 targetPosAvg = targetPositions / hitCount;

            Quaternion targetRot = Quaternion.FromToRotation(transform.up, targetNormalAvg) * transform.rotation;

            if (Quaternion.Angle(targetRot, transform.rotation) > rotationDampening)
            {
                transform.rotation = targetRot;
            }

            Vector3 targetPosition = targetPosAvg + (targetNormalAvg * attachmentOffset.magnitude);
            if ((targetPosition - transform.position).magnitude > positionDampening)
            {
                transform.position = targetPosition;
            }
        }

        hitCountDbg = hitCount;

        Debug.DrawRay(transform.position, transform.up * 2, Color.green);
        Debug.DrawRay(transform.position, transform.forward * 2, Color.blue);
        Debug.DrawRay(transform.position, transform.right * 2, Color.red);
    }
}
