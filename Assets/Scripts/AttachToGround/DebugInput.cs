using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInput : MonoBehaviour
{
    public float moveSpeed = 10;
    public float rotationSpeed = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        transform.Translate(new Vector3(0, 0, verticalInput) * moveSpeed * Time.deltaTime);
        transform.Rotate(new Vector3(0, horizontalInput * rotationSpeed, 0), Space.Self);
    }
}
