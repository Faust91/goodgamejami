using UnityEngine;

public class BoardInteractor : MonoBehaviour, IInteractor
{
    private AOMHelper _aomHelper = null;

    private void Awake()
    {
        if (!TryGetComponent<AOMHelper>(out _aomHelper))
        {
            Debug.LogError("AOM Helper not attached to player");
        }
    }

    public bool TryAddAOM(AreaOfMovementData aom)
    {
        return _aomHelper.TryAddAOM(aom);
    }

}
