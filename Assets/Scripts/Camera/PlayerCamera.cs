﻿using System.Collections;
using UnityEngine;


public class PlayerCamera : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private InputReader myInputReader;


    [SerializeField] private float distance = 10.0f;
    [SerializeField] private float xSpeed = 250.0f;
    [SerializeField] private float ySpeed = 120.0f;
    
    //[SerializeField] private float yMinLimit = -20f;
    //[SerializeField] private float yMaxLimit = 80f;

    private float x = 0.0f;
    private float y = 0.0f;
    private float z = 0.0f;

    [SerializeField] private float wallCamMinDistance = 3f;
    [SerializeField] private float addedDistanceDuringJump = 4f;
    [SerializeField] private AnimationCurve distanceOverZenith;

    [SerializeField] private LayerMask wallLayerMask;

    [SerializeField] private float lateralOffset = 0f;
    [SerializeField] private float depthOffset = 0f;
    [SerializeField] private float verticalOffset = 0f;

    private Transform playerTransform;
    private PlayerController playerController;
    private float standardDistance;
    private float noCameraInputTime = 0f;


    [SerializeField] private float timeToBackToShoulder = 5f;
    [SerializeField] private float backToShoulderMovementTime = 1f;
    [SerializeField] private bool canGoBackToShoulder = true;

    private bool isGoingBackToShoulder = false;
    private float backToShoulderStartY = 0.0f, backToShoulderStartX = 0.0f, backToShoulderStartZ = 0.0f;
    private float backToShoulderElapsedTime = 0f;

    private float speedEpsilon = 0.05f;

    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        playerTransform = target;
        x = angles.y;
        y = angles.x;
        z = angles.z;
        standardDistance = distance;

        playerController = playerTransform.GetComponent<PlayerController>();
        if (!playerController)
            Debug.LogAssertion("Missing Player Contoller on Player Target");
    }

    public Transform CurrentTarget
    {
        get { return target; }
    }

    private void GetCameraInput()
    {
        Vector2 cameraInput = myInputReader.cameraInput;
        float horizontal = cameraInput.x;
        float vertical = cameraInput.y;

        if (Mathf.Abs(horizontal) > 0.5f)
        {
            x += (float)(horizontal * xSpeed * Time.deltaTime);
            noCameraInputTime = 0f;
        }
        else
            noCameraInputTime += Time.deltaTime;

        if (Mathf.Abs(vertical) > 0.5f)
        {
            y += (float)(vertical * ySpeed * Time.deltaTime);
            noCameraInputTime = 0f;
        }
        else
            noCameraInputTime += Time.deltaTime;

        //y = ClampAngle(y, yMinLimit, yMaxLimit);

        canGoBackToShoulder = false; // TEMPORARILY REMOVED playerController.CurrentSpeed < speedEpsilon ||
                              //noCameraInputTime > timeToBackToShoulder;

        if (playerController.CurrentSpeed > speedEpsilon && //Player starts moving
            canGoBackToShoulder)
        {
            isGoingBackToShoulder = true;
            canGoBackToShoulder = false;
            backToShoulderStartY = y;
            backToShoulderStartX = x;
            backToShoulderStartZ = z;
        }

    }

    void UpdatePosition()
    {
        Quaternion rotation;
        Vector3 position;

        if(isGoingBackToShoulder)
        {
            backToShoulderElapsedTime += Time.deltaTime;
            y = Mathf.LerpAngle(y, target.rotation.eulerAngles.x, backToShoulderElapsedTime / backToShoulderMovementTime);
            x = Mathf.LerpAngle(x, target.rotation.eulerAngles.y, backToShoulderElapsedTime / backToShoulderMovementTime);
            z = Mathf.LerpAngle(x, target.rotation.eulerAngles.z, backToShoulderElapsedTime / backToShoulderMovementTime);

            if (backToShoulderElapsedTime > backToShoulderMovementTime)
            {
                backToShoulderElapsedTime = 0f;
                isGoingBackToShoulder = false;
            }
        }

        rotation = Quaternion.Euler(y, x, z);
        transform.rotation = rotation;
       
        RaycastHit hitInfo;
        Vector3 cameraFocusPoint = target.position;
        float cameradistance;

        Vector3 idealCameraPos;

        idealCameraPos = cameraFocusPoint + rotation * new Vector3(0.0f, 0.0f, 0.0f);

        //Debug.DrawRay(defaultTarget.position, idealCameraPos - cameraFocusPoint, Color.green);

        //WALLS COLLISIONS
        if (Physics.Raycast(playerTransform.position, (idealCameraPos - cameraFocusPoint).normalized,
            out hitInfo, Vector3.Distance(playerTransform.position, idealCameraPos), wallLayerMask))
        {

            if (Physics.Raycast(cameraFocusPoint, (idealCameraPos - cameraFocusPoint).normalized,
                out hitInfo, Vector3.Distance(playerTransform.position, idealCameraPos), wallLayerMask))
            {
                distance = Vector3.Distance(cameraFocusPoint, hitInfo.point);
                position = hitInfo.point;//We want the point hit by the cameraFocus, not the character
            }
            else
            {
                distance = Mathf.Lerp(distance, wallCamMinDistance, Time.deltaTime * 10);
                idealCameraPos = cameraFocusPoint + rotation * new Vector3(0.0f, 0.0f, -distance);
                position = idealCameraPos;
            }
            //distance = Vector3.Distance(cameraFocusPoint, hitInfo.point);
            //Debug.DrawLine(hitInfo.point + transform.right, hitInfo.point - transform.right, Color.cyan);
            //Debug.DrawLine(hitInfo.point + transform.up, hitInfo.point - transform.up, Color.cyan);
        }
        //NO WALLS COLLISIONS
        else
        {
            //NORMAL CAMERA DISTANCE            
            cameradistance = distanceOverZenith.Evaluate(zenithAngle());
            distance = Mathf.Lerp(distance, cameradistance, Time.deltaTime * 10);
            idealCameraPos = cameraFocusPoint + rotation * new Vector3(0.0f, 0.0f, -distance);
            position = idealCameraPos;  
        }

        position += (transform.right * lateralOffset) + (transform.up * verticalOffset) + (transform.forward * depthOffset);
        transform.position = position;
    }

    private float zenithAngle()
    {
        float zenithangle = transform.localEulerAngles.x;
        zenithangle = (zenithangle > 180) ? zenithangle - 360 : zenithangle;
        return zenithangle;
    }

    public Transform GetTarget()
    {
        return target;
    }

    public Transform GetDefaultTarget()
    {
        return playerTransform;
    }

    void LateUpdate()
    {
        //Debug.Log(target);
        GetCameraInput();
        UpdatePosition();
    }

    private float ClampAngle(float angle, float min, float max)
    {
        //Debug.Log(angle);

        if (angle < -360)
        {
            angle += 360;
        }
        if (angle > 360)
        {
            angle -= 360;
        }
        return Mathf.Clamp(angle, min, max);
    }
}