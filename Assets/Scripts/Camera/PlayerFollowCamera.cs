using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerFollowCamera : MonoBehaviour
{
    [SerializeField] private GameObject cameraTarget;
    [SerializeField] private float slerpSpeed = 100f;

    void Start()
    {
        if (!cameraTarget)
            Debug.LogAssertion("Missing Player Controller to Follow.");

        transform.position = cameraTarget.transform.position;
        transform.rotation = cameraTarget.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Slerp(transform.position, cameraTarget.gameObject.transform.position, slerpSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, cameraTarget.transform.rotation, slerpSpeed * Time.deltaTime);    
    }
}
