using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ExplorableAreaTrigger : MonoBehaviour, IInteractable
{
    [SerializeField] private GameObject _AOM;

    private AreaOfMovement areaOfMovement = null;

    private void Awake()
    {
        areaOfMovement = _AOM.GetComponent<AreaOfMovement>();
        if (null == areaOfMovement)
        {
            Debug.LogError("AOM not found!");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        IInteractor otherInteractor;
        if (other.gameObject.TryGetComponent<IInteractor>(out otherInteractor))
        {
            Interact(otherInteractor);
        }
    }

    public bool Interact(IInteractor interactor)
    {
        Debug.Log("Interaction triggered!");
        AreaOfMovementData areaOfMovementData = new AreaOfMovementData(areaOfMovement);
        if (interactor.TryAddAOM(areaOfMovementData))
        {
            _AOM.GetComponentInChildren<MeshRenderer>().enabled = true;
            Debug.Log(string.Format("Added area with center: [{0:0.00}, {1:0.00}, {2:0.00}] and radius: [{3:0.00}])", areaOfMovementData.GetCenter().x, areaOfMovementData.GetCenter().y, areaOfMovementData.GetCenter().z, areaOfMovementData.GetRadius()));
            return true;
        }
        return false;
    }

    private void OnDrawGizmos()
    {

        Collider[] colliders = GetComponents<Collider>();
        foreach (Collider collider in colliders)
        {
            if (collider.isTrigger)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, collider.bounds.extents.magnitude);
            }
        }
        Gizmos.color = Color.white;
    }
}
