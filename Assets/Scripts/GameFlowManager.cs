using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameFlowManager : MonoBehaviour
{
    static private Coroutine _runningCoroutine = null;

    [SerializeField] private TextMeshProUGUI _timerGUI;
    [SerializeField] private GameObject _victoryPage;
    [SerializeField] private int _countdownInitialValue = 30;
    private int _countdown;
    private WaitForSeconds _waitFor1Second;

    private void Awake()
    {
        _waitFor1Second = new WaitForSeconds(1.0f);
    }

    void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        _countdown = _countdownInitialValue;
        Time.timeScale = 1.0f;
        if (_runningCoroutine != null)
        {
            StopCoroutine(_runningCoroutine);
        }
        _runningCoroutine = StartCoroutine(CountdownCoroutine());
    }

    IEnumerator CountdownCoroutine()
    {
        while (_countdown > 0)
        {
            _timerGUI.text = _countdown.ToString();
            yield return _waitFor1Second;
            --_countdown;
        }
        _countdown = 0;
        _timerGUI.text = _countdown.ToString();
        Time.timeScale = 0.0f;
        _victoryPage.active = true;
    }

}
