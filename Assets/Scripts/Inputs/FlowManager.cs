using UnityEngine;
using UnityEngine.SceneManagement;


public class FlowManager : MonoBehaviour
{
    public bool enableDebug;
    

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (enableDebug) Debug.Log("Quit");
            Application.Quit();
        }
        
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (enableDebug) Debug.Log("Quit");
            SceneManager.LoadScene(0);
        }
    }
}
