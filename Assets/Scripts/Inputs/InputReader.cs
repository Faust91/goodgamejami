using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


public class InputReader : MonoBehaviour
{
    [Header("Input specs")]
    public UnityEvent changedInputToMouseAndKeyboard;
    public UnityEvent changedInputToGamepad;


    [HideInInspector]
    public Vector2 axisInput;
    [HideInInspector]
    public Vector2 cameraInput = Vector2.zero;
    [HideInInspector]
    public bool isAccelerate = false;

    public bool enableDebug;



    private MovementActions movementActions;
    private bool isMouseAndKeyboard = true;
    private bool oldInput = true;


    /**/


    private void Awake()
    {
        movementActions = new MovementActions();
        movementActions.Gameplay.Movement.performed += ctx => OnMove(ctx);
        movementActions.Gameplay.Camera.performed += ctx => OnCamera(ctx);

        movementActions.Gameplay.Acceleration.performed += ctx => OnAccelerate(ctx);
        movementActions.Gameplay.Acceleration.canceled += ctx => OnAccelerateEnded(ctx);
    }


    private void GetDeviceNew(InputAction.CallbackContext ctx)
    {
        oldInput = isMouseAndKeyboard;

        if (ctx.control.device is Keyboard || ctx.control.device is Mouse) isMouseAndKeyboard = true;
        else isMouseAndKeyboard = false;

        if (oldInput != isMouseAndKeyboard && isMouseAndKeyboard) changedInputToMouseAndKeyboard.Invoke();
        else if (oldInput != isMouseAndKeyboard && !isMouseAndKeyboard) changedInputToGamepad.Invoke();
    }


    #region Actions

    public void OnMove(InputAction.CallbackContext ctx)
    {
        axisInput = ctx.ReadValue<Vector2>();
        GetDeviceNew(ctx);
            
        if(enableDebug) Debug.Log(axisInput);
    }


    public void OnCamera(InputAction.CallbackContext ctx)
    {
        cameraInput = ctx.ReadValue<Vector2>();
        GetDeviceNew(ctx);

        if (enableDebug) Debug.Log(cameraInput);
    }


    public void OnAccelerate(InputAction.CallbackContext ctx)
    {
        isAccelerate = ctx.ReadValueAsButton();

        if (enableDebug) Debug.Log(isAccelerate);
    }

    public void OnAccelerateEnded(InputAction.CallbackContext ctx)
    {
        isAccelerate = ctx.ReadValueAsButton();

        if (enableDebug) Debug.Log(isAccelerate);
    }

    #endregion


    #region Enable / Disable

    private void OnEnable()
    {
        movementActions.Enable();
    }

    private void OnDisable()
    {
        movementActions.Disable();
    }

    #endregion
}