using UnityEngine;

public class LineToAnchorDrawer : MonoBehaviour
{
    private AOMHelper _AOMHelper;
    private LineRenderer _lineRenderer;

    private Vector3[] _postions;

    private void Awake()
    {
        _postions = new Vector3[] { Vector3.zero, Vector3.zero };

        _AOMHelper = GetComponentInParent<AOMHelper>();
        _lineRenderer = GetComponent<LineRenderer>();
    }

    void LateUpdate()
    {
        _postions[0] = transform.position;

        AreaOfMovementData aom = _AOMHelper.GetCurrentAOM();
        if (null != aom)
        {
            _postions[1] = aom.GetCenter();
        }
        else
        {
            _postions[1] = transform.position;
        }
        _lineRenderer.SetPositions(_postions);
    }
}
