using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MushroomInteractionManager : MonoBehaviour
{

    public delegate void OnMushroomInteractionDelegate();
    public event OnMushroomInteractionDelegate OnMushroomInteraction;

    public void HandleInteraction()
    {
        Debug.Log("MushroomInteractionManager::HandleInteraction");
        OnMushroomInteraction?.Invoke();
    }

    void Update()
    {
#if UNITY_EDITOR
        // TODO: remove debug once mushroom is done
        if (Input.GetKeyDown(KeyCode.I))
        {
            HandleInteraction();
        }
#endif
    }
}
