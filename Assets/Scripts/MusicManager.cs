using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private GameObject _mushroomInteractionManagerGO;

    private FMODUnity.StudioEventEmitter _emitter;
    private MushroomInteractionManager _mushroomInteractionManager;

    private int _mushroomsCounter = 0;

    // Start is called before the first frame update
    void Awake()
    {
        _emitter = GetComponent<FMODUnity.StudioEventEmitter>();
        _mushroomInteractionManager = _mushroomInteractionManagerGO.GetComponent<MushroomInteractionManager>();
    }

    private void Start()
    {
        if (_mushroomInteractionManager != null)
        {
            _mushroomInteractionManager.OnMushroomInteraction += IncrementIntensity;
        }
    }

    private void IncrementIntensity()
    {
        ++_mushroomsCounter;
        _emitter.SetParameter("Intensity", _mushroomsCounter);
    }
}
