using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MusicWave : MonoBehaviour
{
    [SerializeField] private float _RPM = 110.0f;
    [SerializeField] private int _fraction = 4;
    [SerializeField] private float _minRadius = 1.0f;
    [SerializeField] private float _maxRadius = 10.0f;

    private Transform _wave = null;
    private float timeCounter = 0.0f;
    private float timeToWave = 0.0f;

    private void Awake()
    {
        _wave = transform;
        timeToWave = _RPM / 60.0f / _fraction;
    }

    private void Start()
    {
        _wave.localScale = CalculateLocalScale(_minRadius);
    }

    private void Update()
    {
        timeCounter += Time.deltaTime;
        timeCounter %= timeToWave;
        float t = timeCounter / timeToWave;
        float newRadius = Mathf.Lerp(_minRadius, _maxRadius, t);
        Vector3 newSCale = CalculateLocalScale(newRadius);
        _wave.localScale = newSCale;
    }

    private Vector3 CalculateLocalScale(float radius)
    {
        return Vector3.one * 2.0f * radius;
    }
}
