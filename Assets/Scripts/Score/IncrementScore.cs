using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncrementScore : MonoBehaviour
{

    [SerializeField] private GameObject _mushroomInteractionManagerGO;
    [SerializeField] private int _scorePerMushroom;

    private MushroomInteractionManager _mushroomInteractionManager;
    private Score _score;

    // Start is called before the first frame update
    void Start()
    {
        _mushroomInteractionManager = _mushroomInteractionManagerGO.GetComponent<MushroomInteractionManager>();
        if (_mushroomInteractionManager != null)
        {
            _mushroomInteractionManager.OnMushroomInteraction += incrementScore;
        }
        else
        {
            Debug.LogError("Trying to start IncrementScore component without a ref to a GameObject with MushroomInteractionManager component");
        }

        _score = GetComponent<Score>();
        if (_score == null)
        {
            Debug.LogError("Trying to start IncrementScore component in a GameObject without Score component");
        }
    }

    private void incrementScore()
    {
        if (_score != null)
        {
            _score.AddScore(_scorePerMushroom);
        }
    }
}
