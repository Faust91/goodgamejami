using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _titleScreenBestScore;
    [SerializeField] TextMeshProUGUI _gameplayLevelCurrentScore;
    [SerializeField] TextMeshProUGUI _victoryScreenCurrentScore;
    [SerializeField] TextMeshProUGUI _victoryScreenBestScore;

    public int CurrentScore { get; private set; }
    private static int _bestScore = 0;

    void Start()
    {
        CurrentScore = 0;

        updateBestScoreText();
        updateCurrentScoreText();
    }

    public void AddScore(int score)
    {
        CurrentScore += score;
        updateCurrentScoreText();
        updateBestScore();
    }

    private void updateBestScore()
    {
        if (CurrentScore > _bestScore)
        {
            _bestScore = CurrentScore;
            updateBestScoreText();
        }
    }

    private void updateCurrentScoreText()
    {
        if (_gameplayLevelCurrentScore != null)
        {
            _gameplayLevelCurrentScore.text = CurrentScore.ToString();
        }
        if (_victoryScreenCurrentScore != null)
        {
            _victoryScreenCurrentScore.text = "SCORE: " + CurrentScore.ToString();
        }

    }

    private void updateBestScoreText()
    {
        if (_titleScreenBestScore != null)
        {
            if (_bestScore == 0)
            {
                _titleScreenBestScore.text = "";
            }
            else
            {
                _titleScreenBestScore.text = "BEST SCORE: " + _bestScore.ToString();
            }
        }
        if (_victoryScreenBestScore != null)
        {
            _victoryScreenBestScore.text = "BEST SCORE: " + _bestScore.ToString();
        }
    }
}
