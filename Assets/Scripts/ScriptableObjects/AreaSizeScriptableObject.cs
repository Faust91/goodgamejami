using UnityEngine;

[CreateAssetMenu(fileName = "AreaSize", menuName = "ScriptableObjects/AreaSizeScriptableObject", order = 1)]
public class AreaSizeScriptableObject : ScriptableObject
{
    public float radius = 0.0f;

    public override string ToString()
    {
        return string.Format("radius:[{1:0.00}]", radius);
    }
}
