using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SelfDestruction : MonoBehaviour
{
    public GameObject parentObj;


    public void Destroy()
    {
        GameObject.Destroy(parentObj);
    }
}
