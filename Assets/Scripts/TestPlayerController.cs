using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayerController : MonoBehaviour
{
    [SerializeField] private float speed = 1.0f;
    [SerializeField] private float rotationSpeed = 1.0f;

    void Update()
    {
        Vector3 nextPosition = transform.position;
        if (Input.GetKey(KeyCode.W))
        {
            nextPosition.z += speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            nextPosition.z -= speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            nextPosition.x += speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            nextPosition.x -= speed * Time.deltaTime;
        }
        transform.position = nextPosition;

        Quaternion nextRotation = transform.rotation;
        if (Input.GetKey(KeyCode.RightArrow))
        {
            nextRotation *= Quaternion.Euler(Vector3.up * rotationSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            nextRotation *= Quaternion.Euler(Vector3.up * -rotationSpeed * Time.deltaTime);
        }
        transform.rotation = nextRotation;
    }
}
