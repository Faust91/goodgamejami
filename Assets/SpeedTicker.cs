using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedTicker : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tickGatherer();
    }

    private void tickGatherer()
    {
        if (HasGathered)
        {
            HasGathered = false;
            currentTime = 0;
        }
        currentTime += Time.deltaTime;
        if(currentTime >= GatheringDeltaTime)
        {
            HasGathered = true;
        }
    }

    public bool HasGathered;
    public float GatheringDeltaTime;
    public float SpeedIncrease;
    public float currentTime = 0f;
}
