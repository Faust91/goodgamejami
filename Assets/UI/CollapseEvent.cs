using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CollapseEvent : MonoBehaviour
{
    public GameObject parentObj;


    public void CollapseParent()
    {
        parentObj.SetActive(false); 
    }
}
