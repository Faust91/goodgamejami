using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject pauseUI;
    [SerializeField] private int titleScreenScene;

    // Start is called before the first frame update
    void Start()
    {
        if(!pauseUI)
        {
            Debug.LogAssertion("Missing pauseUI reference.");
        }
        Time.timeScale = 1;
        pauseUI.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("START"))
        {
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                pauseUI.gameObject.SetActive(false);
            }
            else
            {
                Time.timeScale = 0;
                pauseUI.gameObject.SetActive(true);
            }
        }
        else if (Time.timeScale == 0)
        {
            if (Input.GetButtonDown("A"))
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(titleScreenScene);
            }
            else if (Input.GetButtonDown("B"))
                SceneLoader.QuitGame();
        }
    }
}
