using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    [SerializeField] private int titleScreenScene = 0;


    private void Update()
    {
        if (Input.GetButtonDown("A")) SceneManager.LoadScene(titleScreenScene);
    }
}
